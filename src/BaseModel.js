var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var basename = path.basename(module.filename);
var env = process.env.NODE_ENV || 'development';
var config = require('./config')[env];

var db = {};

var sequelize = new Sequelize(config);

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

export default class BaseModel {
  constructor(name, connection) {
    this.name = name;
    if (sequelize) {
      this.connection = sequelize;
    }
  }

   async createConnection(){
    
      if (sequelize) {
        this.connection = sequelize;
        return this.connection;
      }
      
   }

  
  async _getModel() {
    this.model = await this.connection.model(this.name, this.schema);
  }
}
