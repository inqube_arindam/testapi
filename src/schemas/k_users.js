'use strict';
module.exports = (sequelize, DataTypes) => {
  const k_users = sequelize.define('k_users', {
    name: DataTypes.STRING,
    e_mail: DataTypes.STRING,
    phone: DataTypes.INTEGER,
    file_path:DataTypes.STRING,
  }, {});
  k_users.associate = function(models) {
    // associations can be defined here
  };
  return k_users;
};