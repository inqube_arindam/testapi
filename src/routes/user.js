import { route, successRoute } from "./";
import userModel from "../model/userModel";
export const createuser = route(async (req, res) => {
    const {
        name,
        e_mail,
        phone,
    } = req.body
    let file = req.file
    const output = await userModel.createuser(name, e_mail, phone, file)
    res.send(await successRoute(output));
})
export const updateuser = route(async (req, res) => {
    const {
        name,
        e_mail,
        phone,
    } = req.body
    let file = req.file
    let id = req.params.id
    const output = await userModel.updateuser(name, e_mail, phone, file, id)
    res.send(await successRoute(output));
})