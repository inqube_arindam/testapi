import "babel-polyfill";
const express = require('express');
const app = express();
import { getEnv } from "./lib/env";
import https from "https";
import http from "http";
import createRouter from "./router";
const cors = require("cors");
const isProduction = getEnv("NODE_ENV") === "production";
app.use(cors());
if (isProduction) {
  const sslOptions = {
    key: fs.readFileSync(getEnv("PRIVKEY_CERT_LOC", true)),
    cert: fs.readFileSync(getEnv("FULLCHAIN_CERT_LOC", true))
  };
  http.createServer(app).listen(80);
  https.createServer(sslOptions, app).listen(443);
} else {  
  const port = getEnv("PORT");
  http
    .createServer(app)
    .listen(port, () => console.log(`Listening on port ${port}`));
}
// Call routers
app.use(createRouter());