import express from "express";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import path from "path";
import { ApplicationError } from "./lib/errors";
var multipart = require('connect-multiparty');

const userRoute = require('./preroutes/userRoute');


export default function createRouter() {
    const router = express.Router();
    router.use(cookieParser()); // parse cookies automatically
    // router.use(require('body-parser').urlencoded());
    router.use(bodyParser.urlencoded({ extended: false }));
    router.use(bodyParser.json());
    // router.use(multipart());

    router.get("/*", (req, res, next) => {
        res.set({
            "Last-Modified": new Date().toUTCString(),
            Expires: -1,
            "Cache-Control": "must-revalidate, private"
        });
        next();
    });

   router.use('/userRoute', userRoute);
 
    //=================Error Handling==========================
    // 404 route
    router.all("/*", (req, res, next) => {
        next(new ApplicationError("Not Found", 404));
    });

    router.use((err, req, res, next) => {
        if (err instanceof ApplicationError) {
            res.status(err.statusCode).send({
                // message: err.message,
                data: err.data || {},
                error: { errorMessage: err.message, errorCode: err.statusCode }
            });
            return;
        }
        console.error(err);

        res.status(500).send({
            // message: "Uncaught error"
            data: err.data || {},
            error: { errorMessage: "Something went wrong", errorCode: 500 }
        }); // uncaught exception
        // res.status(200).send({
        //   error: { errorMessage: "Success", errorCode: 200 },
        //   data: err.data || {}
        // });
    });
    return router;
}