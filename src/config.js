const dotenv = require('dotenv');
const result = dotenv.config();

if (result.error) {
  throw result.error
}


module.exports = {
  development: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE_NAME,
    host: process.env.DB_HOST,
    host: "localhost",
    dialect: 'mysql',
    timezone: '+05:30',

    pool: {
      maxConnections: 15,
      minConnections: 5,
      maxIdleTime: 10000
    }

  },
  test: {
    username: "root",
    password: "root",
    database: "testapi",
    host: "localhost",
    dialect: 'mysql',
    timezone: '+05:30',
  },
  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE_NAME,
    host: process.env.DB_HOST,
    dialect: 'mysql',
  }
}