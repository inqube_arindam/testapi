const express = require('express');
const router = express.Router();
var multer = require('multer');
import { file_upload } from "../lib/common";
const path = require('path');
const getdest = file_upload("uploads/user/");
const upload = multer({ storage: getdest });
import {
    createuser as createuser,
    updateuser as updateuser
} from "../routes/user";
router.post('/createuser', upload.single('photo'), createuser)
router.put('/updateuser/:id', upload.single('photo'), updateuser)
module.exports = router;