'use strict';
const uuidv1 = require("uuid/v1");
const bycrupt = require("bcryptjs");

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "bk_roles",
      [
        {
          role_id: 10,
          roleName: "Developer",
          roleSlug: "developer",
          roleDescription: "developer",
          modulePermission: '[{"id": 1, "name": "create_user", "value": 1}, {"id": 2, "name": "list_user", "value": 1}, {"id": 3, "name": "view_user", "value": 1}, {"id": 4, "name": "update_user", "value": 1}, {"id": 6, "name": "create_category", "value": 1}, {"id": 7, "name": "list_category", "value": 1}, {"id": 8, "name": "view_category", "value": 1}, {"id": 9, "name": "update_category", "value": 1}, {"id": 11, "name": "create_author", "value": 1}, {"id": 12, "name": "list_author", "value": 1}, {"id": 13, "name": "view_author", "value": 1}, {"id": 14, "name": "update_author", "value": 1}, {"id": 16, "name": "import_author", "value": 1}, {"id": 17, "name": "create_publisher", "value": 1}, {"id": 18, "name": "list_publisher", "value": 1}, {"id": 19, "name": "view_publisher", "value": 1}, {"id": 20, "name": "update_publisher", "value": 1}, {"id": 22, "name": "create_book", "value": 1}, {"id": 23, "name": "list_book", "value": 1}, {"id": 24, "name": "view_book", "value": 1}, {"id": 25, "name": "update_book", "value": 1}, {"id": 27, "name": "import_book", "value": 1}, {"id": 28, "name": "create_banner", "value": 1}, {"id": 29, "name": "list_banner", "value": 1}, {"id": 30, "name": "view_banner", "value": 1}, {"id": 31, "name": "update_Banner", "value": 1}, {"id": 32, "name": "list_comments", "value": 1}]',
          status: "1"
        },
        {
          role_id: 1,
          roleName: "Admin",
          roleSlug: "admin",
          roleDescription: "admin",
          modulePermission: '[{"id": 1, "name": "create_user", "value": 1}, {"id": 2, "name": "list_user", "value": 1}, {"id": 3, "name": "view_user", "value": 1}, {"id": 4, "name": "update_user", "value": 1}, {"id": 6, "name": "create_category", "value": 1}, {"id": 7, "name": "list_category", "value": 1}, {"id": 8, "name": "view_category", "value": 1}, {"id": 9, "name": "update_category", "value": 1}, {"id": 11, "name": "create_author", "value": 1}, {"id": 12, "name": "list_author", "value": 1}, {"id": 13, "name": "view_author", "value": 1}, {"id": 14, "name": "update_author", "value": 1}, {"id": 16, "name": "import_author", "value": 1}, {"id": 17, "name": "create_publisher", "value": 1}, {"id": 18, "name": "list_publisher", "value": 1}, {"id": 19, "name": "view_publisher", "value": 1}, {"id": 20, "name": "update_publisher", "value": 1}, {"id": 22, "name": "create_book", "value": 1}, {"id": 23, "name": "list_book", "value": 1}, {"id": 24, "name": "view_book", "value": 1}, {"id": 25, "name": "update_book", "value": 1}, {"id": 27, "name": "import_book", "value": 1}, {"id": 28, "name": "create_banner", "value": 1}, {"id": 29, "name": "list_banner", "value": 1}, {"id": 30, "name": "view_banner", "value": 1}, {"id": 31, "name": "update_Banner", "value": 1}, {"id": 32, "name": "list_comments", "value": 1}]',
          status: "1"
        },
        {
          role_id: 2,
          roleName: "Customer",
          roleSlug: "customer",
          roleDescription: "customer",
          modulePermission: '[{"id": 1, "name": "create_user", "value": 1}, {"id": 2, "name": "list_user", "value": 1}, {"id": 3, "name": "view_user", "value": 1}, {"id": 4, "name": "update_user", "value": 1}, {"id": 6, "name": "create_category", "value": 1}, {"id": 7, "name": "list_category", "value": 1}, {"id": 8, "name": "view_category", "value": 1}, {"id": 9, "name": "update_category", "value": 1}, {"id": 11, "name": "create_author", "value": 1}, {"id": 12, "name": "list_author", "value": 1}, {"id": 13, "name": "view_author", "value": 1}, {"id": 14, "name": "update_author", "value": 1}, {"id": 16, "name": "import_author", "value": 1}, {"id": 17, "name": "create_publisher", "value": 1}, {"id": 18, "name": "list_publisher", "value": 1}, {"id": 19, "name": "view_publisher", "value": 1}, {"id": 20, "name": "update_publisher", "value": 1}, {"id": 22, "name": "create_book", "value": 1}, {"id": 23, "name": "list_book", "value": 1}, {"id": 24, "name": "view_book", "value": 1}, {"id": 25, "name": "update_book", "value": 1}, {"id": 27, "name": "import_book", "value": 1}, {"id": 28, "name": "create_banner", "value": 1}, {"id": 29, "name": "list_banner", "value": 1}, {"id": 30, "name": "view_banner", "value": 1}, {"id": 31, "name": "update_Banner", "value": 1}, {"id": 32, "name": "list_comments", "value": 1}]',
          status: "1"
        },
        {
          role_id: 3,
          roleName: "Author",
          roleSlug: "author",
          roleDescription: "author",
          modulePermission: '[{"id": 1, "name": "create_user", "value": 1}, {"id": 2, "name": "list_user", "value": 1}, {"id": 3, "name": "view_user", "value": 1}, {"id": 4, "name": "update_user", "value": 1}, {"id": 6, "name": "create_category", "value": 1}, {"id": 7, "name": "list_category", "value": 1}, {"id": 8, "name": "view_category", "value": 1}, {"id": 9, "name": "update_category", "value": 1}, {"id": 11, "name": "create_author", "value": 1}, {"id": 12, "name": "list_author", "value": 1}, {"id": 13, "name": "view_author", "value": 1}, {"id": 14, "name": "update_author", "value": 1}, {"id": 16, "name": "import_author", "value": 1}, {"id": 17, "name": "create_publisher", "value": 1}, {"id": 18, "name": "list_publisher", "value": 1}, {"id": 19, "name": "view_publisher", "value": 1}, {"id": 20, "name": "update_publisher", "value": 1}, {"id": 22, "name": "create_book", "value": 1}, {"id": 23, "name": "list_book", "value": 1}, {"id": 24, "name": "view_book", "value": 1}, {"id": 25, "name": "update_book", "value": 1}, {"id": 27, "name": "import_book", "value": 1}, {"id": 28, "name": "create_banner", "value": 1}, {"id": 29, "name": "list_banner", "value": 1}, {"id": 30, "name": "view_banner", "value": 1}, {"id": 31, "name": "update_Banner", "value": 1}, {"id": 32, "name": "list_comments", "value": 1}]',
          status: "1"
        },
        {
          role_id: 4,
          roleName: "Publisher",
          roleSlug: "publisher",
          roleDescription: "publisher",
          modulePermission: '[{"id": 1, "name": "create_user", "value": 1}, {"id": 2, "name": "list_user", "value": 1}, {"id": 3, "name": "view_user", "value": 1}, {"id": 4, "name": "update_user", "value": 1}, {"id": 6, "name": "create_category", "value": 1}, {"id": 7, "name": "list_category", "value": 1}, {"id": 8, "name": "view_category", "value": 1}, {"id": 9, "name": "update_category", "value": 1}, {"id": 11, "name": "create_author", "value": 1}, {"id": 12, "name": "list_author", "value": 1}, {"id": 13, "name": "view_author", "value": 1}, {"id": 14, "name": "update_author", "value": 1}, {"id": 16, "name": "import_author", "value": 1}, {"id": 17, "name": "create_publisher", "value": 1}, {"id": 18, "name": "list_publisher", "value": 1}, {"id": 19, "name": "view_publisher", "value": 1}, {"id": 20, "name": "update_publisher", "value": 1}, {"id": 22, "name": "create_book", "value": 1}, {"id": 23, "name": "list_book", "value": 1}, {"id": 24, "name": "view_book", "value": 1}, {"id": 25, "name": "update_book", "value": 1}, {"id": 27, "name": "import_book", "value": 1}, {"id": 28, "name": "create_banner", "value": 1}, {"id": 29, "name": "list_banner", "value": 1}, {"id": 30, "name": "view_banner", "value": 1}, {"id": 31, "name": "update_Banner", "value": 1}, {"id": 32, "name": "list_comments", "value": 1}]',
          status: "1"
        },

      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("bk_roles", null, {});
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};