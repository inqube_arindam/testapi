"use strict";
const uuidv1 = require("uuid/v1");
const bycrupt = require("bcryptjs");

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "bk_dbusers",
      [
        {
          userId: uuidv1(),
          userName: "Admin",
          emailId: "test@admin.com",
          contactNumber: "9876543210",
          password: bycrupt.hashSync("12345678", 10),
          lan_id:"1",
          userType: "1",
          createdBy: "",
          status: "1"
        },
        {
          userId: uuidv1(),
          userName: "Developer",
          emailId: "test@developer.com",
          contactNumber: "1234567890",
          password: bycrupt.hashSync("1234567890", 10),
          lan_id:"1",
          userType: "10",//  Developer UserType status 10
          createdBy: "",
          status: "1"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("bk_dbusers", null, {});
  }
};
