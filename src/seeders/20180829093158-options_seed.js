'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('bk_options', [
      {
        option_name: 'kg_unit',
        option_value: 'kg',
        status: 1
      },
      {
        option_name: 'gm_unit',
        option_value: 'gm',
        status: 1
      },
      {
        option_name: 'pcs_unit',
        option_value: 'pcs',
        status: 1
      },
      {
        option_name: 'discount_type',
        option_value: '1',
        status: 1
      },
      {
        option_name: 'product',
        option_value: '[{"type":"select","label":"Reading Level Min Age","name":"meta_reading_level_min_age","options":[{"id":"10","name":"10"},{"id":"20","name":"20"},{"id":"30","name":"30"},{"id":"40","name":"40"},{"id":"50","name":"50"},{"id":"60","name":"60"},{"id":"70","name":"70"},{"id":"80","name":"80"},{"id":"90","name":"90"}],"fstatus":1,"required":false,"order":"8"},{"type":"select","label":"Reading Level Max Age","name":"meta_reading_level_max_age","options":[{"id":"10","name":"10"},{"id":"20","name":"20"},{"id":"30","name":"30"},{"id":"40","name":"40"},{"id":"50","name":"50"},{"id":"60","name":"60"},{"id":"70","name":"70"},{"id":"80","name":"80"},{"id":"90","name":"90"}],"fstatus":1,"required":false,"order":"9"},{"type":"select","label":"Binding","name":"meta_binding","options":[{"id":"Paperback","name":"Paperback"},{"id":"Hard Binding","name":"Hard Binding"}],"fstatus":1,"required":false,"order":"10"},{"type":"select","label":"Reading Language","name":"meta_language","options":[{"id":"Bengali","name":"Bengali"},{"id":"English","name":"English"}],"fstatus":1,"required":true,"order":"11"},{"type":"input","label":"ISBN-10","inputType":"text","name":"meta_ISBN-10","fstatus":1,"required":false,"order":"12"},{"type":"input","label":"ISBN-13","inputType":"text","name":"meta_ISBN-13","fstatus":1,"required":false,"order":"13"},{"type":"input","label":"Product Dimensions (Height x Width x Length)","inputType":"text","name":"meta_product_dimensions","fstatus":1,"required":false,"order":"14"}]',
        status: 1,
        option_type: 'cf'
      },
      {
        option_name: 'product_category',
        option_value: '[{"label":"Category Name","name":"categoryName","type":"input","inputType":"text","required":true,"fstatus":1,"order":"0"},{"label":"Category Description","name":"description","type":"textArea","inputType":"text","required":true,"fstatus":1,"order":"1"},{"label":"Size","name":"meta_size","type":"input","inputType":"text","required":false,"fstatus":1,"order":"2"},{"label":"Weight","name":"meta_weight","type":"input","inputType":"text","required":false,"fstatus":1,"order":"3"},{"label":"Color","name":"meta_color","type":"input","inputType":"text","required":false,"fstatus":1,"order":"4"},{"label":"Image","name":"photo","type":"input","inputType":"file","required":false,"fstatus":1,"order":"5"},{"label":"Height","name":"meta_height","type":"input","inputType":"text","required":false,"order":"6"},{"label":"Short Description","name":"meta_short_description","type":"textArea","inputType":"text","required":false,"order":"7"},{"label":"New Size","name":"new_size","type":"input","inputType":"text","fstatus":"0","required":false,"options":"","order":"8"},{"label":"Status","name":"status","type":"select","inputType":"text","fstatus":"1","required":null,"options":[{"id":"1","name":"Active"},{"id":"0","name":"Inactive"}],"order":"9"},{"label":"Test Select Type","name":"meta_test_select_type","type":"select","inputType":"text","fstatus":"0","required":false,"options":[{"id":"1","name":"Test Red"},{"id":"2","name":"Test Blue"}],"order":"10"},{"label":"Select Size","name":"meta_select_size","type":"select","inputType":"text","required":false,"fstatus":1,"options":[{"id":"1","name":"Test Small"},{"id":"2","name":"Test Midi um"},{"id":"3","name":"Test Big"}],"order":"11"},{"label":"New Meta Field","name":"meta_new_field","type":"input","inputType":"text","required":false,"fstatus":1,"options":"","order":"11"}]',
        status: 1,
        option_type: 'cf'
      },
      {
        option_name: 'tag',
        option_value: '[{"label":"Tag Name","name":"categoryName","type":"input","inputType":"text","required":true,"fstatus":1,"order":"0"},{"label":"Tag Description","name":"description","type":"textArea","inputType":"text","required":true,"fstatus":1,"order":"1"},{"label":"Image","name":"photo","type":"input","inputType":"file","required":false,"fstatus":1,"order":"2"},{"label":"Status","name":"status","type":"select","inputType":"text","fstatus":"1","required":null,"options":[{"id":"1","name":"Active"},{"id":"0","name":"Inactive"}],"order":"3"}]',
        status: 1,
        option_type: 'cf'
      },
      {
        option_name: 'user',
        option_value: '[{ "type": "input", "label": "First Name", "inputType": "text", "name": "meta_first_name", "required": true,"fstatus":1 }, { "type": "input", "label": "Middle Name", "inputType": "text", "name": "meta_middle_name", "required": false,"fstatus":1 }, { "type": "input", "label": "Last Name", "inputType": "text", "name": "meta_last_name", "required": false,"fstatus":1 }, { "type": "input", "label": "Public Display Name", "inputType": "text", "name": "userName", "required": true,"fstatus":1 }, { "type": "input", "label": "Email Id", "inputType": "text", "name": "emailId", "required": true,"fstatus":1 }, { "type": "input", "label": "Contact Number", "inputType": "text", "name": "contactNumber", "required": true,"fstatus":1 }, { "label": "Password", "name": "password", "type": "input", "inputType": "password", "required": true,"fstatus":1 }, { "label": "Confirm Password", "name": "password", "type": "input", "inputType": "password", "required": true,"fstatus":1 }, { "label": "City", "name": "meta_city", "type": "input", "inputType": "text", "required": false,"fstatus":1 }, { "label": "Country", "name": "meta_country", "type": "input", "inputType": "text", "required": false,"fstatus":1 }, { "label": "Pin", "name": "meta_pin", "type": "input", "inputType": "text", "required": false,"fstatus":1 }, { "label": "Upload photo", "name": "photo", "type": "input", "inputType": "file", "required": false,"fstatus":1 }]',
        status: 1,
        option_type: 'cf'
      },
      {
        option_name: 'publisher',
        option_value: '[{ "type": "input", "label": "Name", "inputType": "text", "name": "userName", "required": true, "order": "0" }, { "type": "input", "label": "Contact Person (First Name)", "inputType": "text", "name": "meta_first_name", "required": true, "order": "1" }, { "type": "input", "label": "Contact Person (Middle Name)", "inputType": "text", "name": "meta_middle_name", "required": false, "order": "2" }, { "type": "input", "label": "Contact Person (Last Name)", "inputType": "text", "name": "meta_last_name", "required": false, "order": "3" }, { "type": "input", "label": "Contact Number", "inputType": "text", "name": "contactNumber", "required": false, "order": "4" }, { "type": "textArea", "label": "Address", "inputType": "text", "name": "meta_address", "required": false, "order": "5" }, { "label": "Description", "name": "meta_description", "type": "textArea", "inputType": "password", "required": false, "order": "6" }, { "label": "Image", "name": "photo", "type": "input", "inputType": "file", "required": false, "order": "7" }]',
        status: 1,
        option_type: 'cf'
      },
      {
        option_name: 'author',
        option_value: '[{"type": "date", "label": "Birth Date", "inputType": "text", "name": "meta_birth_date", "required": true,"fstatus":1 },{"type": "input", "label": "Birth Place", "inputType": "text", "name": "meta_birth_place", "required": false,"fstatus":1 },{"type": "date", "label": "Death Date", "inputType": "text", "name": "meta_death_date", "required": false,"fstatus":1 },{"type": "input", "label": "Death Place", "inputType": "text", "name": "meta_death_place", "required": false,"fstatus":1 },{"label":"Biography","name":"meta_biography","type":"textArea","inputType":"text","required":false,"fstatus":1,"order":"1"}]',
        status:1,
        option_type: 'cf'
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('bk_options', null, {});
  }
};
