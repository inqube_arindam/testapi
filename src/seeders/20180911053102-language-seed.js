"use strict";

var x = require("sequelize");
let seq = new x(require("../config")[process.env.NODE_ENV || "development"]);
var TableNames = seq["import"]("../lib/TableConfig");

// var TableNames=require('../lib/TableConfig');
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      TableNames.Language,
      [
        {
          id: 1,
          name: "ENGLISH",
          sname: "EN"
        },
        {
          id: 2,
          name: "BENGALI",
          sname: "BN"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(TableNames.Language, null, {});
  }
};
