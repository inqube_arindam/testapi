const Models = require('../schemas');
import { user_UPLOAD_URL } from "../lib/constants";
const UserModel = Models.sequelize.model("k_users", Models);
export class userDB {
    createuser = async (name, e_mail, phone, file) => {
        try {
            if (file) {
                var file_path = user_UPLOAD_URL.localUrl + file.path
            }
            else {
                var file_path = user_UPLOAD_URL.localUrl + "uploads/user/" +
                    "noimage.jpg"
            }
            let userInfo = UserModel.create({ name: name, e_mail: e_mail, phone: phone, file_path: file_path }).then(async (result) => {
                return result
            })
            return userInfo
        }
        catch (err) {
            throw err
        }
    }
    updateuser = async (name, e_mail, phone, file, id) => {
        try {
            if (file) {
                var Obj = {
                    name: name,
                    e_mail: e_mail,
                    phone: phone,
                    file_path: user_UPLOAD_URL.localUrl + file.path
                }
            }
            else {
                var Obj = {
                    name: name,
                    e_mail: e_mail,
                    phone: phone,
                }
            }
            let userInfo = UserModel.update(Obj, { where: { id: id } }).then(async (result) => {
                return result
            })
            return userInfo
        }
        catch (err) {
            throw err
        }
    }
}
export default new userDB();