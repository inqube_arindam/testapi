const multer = require('multer');
const path = require('path');
const fs=require('fs')
export function file_upload(upload_dest) {
    if (upload_dest) {
        var storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, upload_dest)
            },
            filename: function (req, file, cb) {
                var dir = path.join(__dirname + '/../../' + upload_dest + '/');;

                if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir);
                }
                cb(null, Date.now() + '-' + file.originalname)
                // cb(null, Date.now() + path.extname(file.originalname))
            }
        })
        return storage;
    }
}

